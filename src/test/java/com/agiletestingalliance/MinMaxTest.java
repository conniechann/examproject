package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {

    @Test
    public void minMaxTest() throws Exception {
        int result = new MinMax().number(4,5);
        assertEquals("Error", 5, result);
    }

    @Test
    public void minMaxTest1() throws Exception {
        int result = new MinMax().number(5,4);
        assertEquals("Error", 5, result);
    }

      @Test
    public void minMaxTest2() throws Exception {
       String result = new MinMax().bar("minmaxtest");
        assertTrue("Error", result.contains("minmaxtest"));
    }

}