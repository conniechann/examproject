package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;


public class AboutCPDOFTest {

    @Test
    public void testAbout() throws Exception {
        String result = new AboutCPDOF().desc();
        assertTrue ("Error", result.contains("DevOps"));


    }
}
